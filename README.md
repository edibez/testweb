## How to run the examnple 

- install node js version 8.11.4 (another version is okay but can have compatibility issue) - https://nodejs.org/dist/latest-v8.x/
- install npm version 5.6.0 (another version is okay but can have compatibility issue) - 
- clone the git
- Goto folder src
- run command : npm install
- Goto cloned root folder
- run command : npm install
- still on the root folder, run command : npm install gulp 
- On root folder , run this command : gulp
- A browser will open 

Additional note :


## Build 

In order to build the production version of your project run __gulp build__ from the root of cloned repo.

## List of npm packaged used

- gulp
- browser-sync
- gulp-sass
- gulp-sourcemaps
- gulp-autoprefixer
- gulp-clean-css
- gulp-uglify
- gulp-concat
- gulp-imagemin
- gulp-changed
- gulp-html-replace
- gulp-htlmin
- del
- run-sequence

Big thanks to all the authors of these packages :heart:

## If you have problem setting up the environment in your local computer, you can use just the full html/css template in folder html.
